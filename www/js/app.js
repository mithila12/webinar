// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('webinar', ['ionic','ngCordova','valdr','app.core','app.login','app.home'])

.run(initApp);

function initApp($ionicPlatform, $cordovaSQLite, sessionConstant, $state, dbconfig, repositoryservice,$ionicHistory,$timeout) {
        $ionicPlatform.ready(function () {
            //for reminders
            if (device.platform === "iOS") {
                window.plugin.notification.local.promptForPermission();
            }
            if (window.cordova && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                cordova.plugins.Keyboard.disableScroll(true);

                var db = $cordovaSQLite.openDB({name: dbconfig.name, iosDatabaseLocation: 'default'});
                sessionConstant.db = db;
                var numTables = dbconfig.tables.length;
                var i = 0;

                angular.forEach(dbconfig.tables, function (table) {
                    var columns = [];
                    angular.forEach(table.columns, function (column) {
                        console.log("table : colume names :: " + table.name + " : " + column);
                        columns.push(column.name + ' ' + column.type);
                    });

                    var dbQuery = 'CREATE TABLE IF NOT EXISTS ' + table.name + ' (' + columns.join(',') + ')';
                    console.log("Database Queries :: " + dbQuery);

                    $cordovaSQLite.execute(db, dbQuery)
                        .then(
                            function (res) {
                                i++;
                                if (i == numTables) {
                                    //$state.go('login');
                                      return repositoryservice.select("APP_CONFIG", " where id>=?", [0]).then(function (result) {
                                          console.log(result);
                                          /*if (result.rows.item(0)['reference_id'] && result.rows.item(0)['loginId']) {
                                            console.log(result.rows.item(0)['reference_id']);
                                            console.log(result.rows.item(0)['loginId']);
                                            sessionConstant.login.cert = result.rows.item(0)['reference_id'];
                                            sessionConstant.login.id = result.rows.item(0)['loginId'];
                                            $state.go('tabs.home');
                                          } else
                                          {
                                              $state.go('login');
                                          }*/
                                          $state.go('login');
                                      }, function (err) {
                                        console.log("Error At Ready : " + err.message);
                                      });
                                      //if true go tologin
                                    }
                                    else {
                                      $state.go('login');
                                    }

                            }
                            , function (err) {
                                console.log("Error At Ready : " + err.message);
                                alert(dbQuery);
                            }
                        );
                });
            }
            if (window.StatusBar) {
                    StatusBar.styleDefault();

                 }
            var backbutton=0;
            $ionicPlatform.registerBackButtonAction(function (event) {
                event.preventDefault();
                if ($state.current.name == "tabs.home") {
                    if(backbutton==0){
                        backbutton++;
                        window.plugins.toast.showShortCenter('Press again to exit');
                        console.log('Press again to exit');
                        $timeout(function(){backbutton=0;},5000);
                    }else{
                        navigator.app.exitApp();
                    }
                } else {
                    $ionicHistory.nextViewOptions({ disableBack: true });
                    $state.go('tabs.home');
                }
            }, 800);//registerBackButton
        })
    }
