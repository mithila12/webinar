(function () {
    'use strict';

    angular.module('webinar')
        .config(function ($stateProvider, $urlRouterProvider) {
            $stateProvider
                .state('tabs', {
                  url: "/tab",
                  abstract: true,
                  templateUrl: "templates/tabs.html"
                })
                .state('tabs.home', {
                  url: "/home",
                  views: {
                    'live-tab': {
                      templateUrl: "templates/home/home.html",
                      controller: 'homectrl',
                      controllerAs: 'vm'

                    }
                  }
                })
                .state('tabs.upcoming', {
                  url: "/upcoming",
                  views: {
                    'upcoming-tab': {
                      templateUrl: "templates/home/upcoming.html",
                      controller: 'homectrl',
                      controllerAs: 'vm'
                    }
                  }
                })
                .state('login', {
                    url: '/login',
                    templateUrl: 'templates/login/login.html',
                    controller: 'loginctrl',
                    controllerAs: 'vm'
                })
                .state('details', {
                    url: '/details',
                    templateUrl: 'templates/home/details.html',
                    controller: 'homectrl',
                    controllerAs: 'vm'
                })
                .state('play', {
                    url: '/play',
                    templateUrl: 'templates/home/videoPlayer.html',
                    controller: 'homectrl',
                    controllerAs: 'vm'
                })



        })
})();
