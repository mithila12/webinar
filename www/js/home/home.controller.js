(function () {
    'use strict';

    angular
        .module('app.home')
        .controller('homectrl', homectrl);

    function homectrl($ionicSideMenuDelegate, $sce, homeservice,sessionConstant,$state, $ionicHistory, $window) {
        var vm = this;
        vm.name = "Pralok";
        vm.printMe = printMe;
        vm.toggleMenu = toggleMenu;
        vm.getMessages = getMessages;
        vm.getLiveVideos = getLiveVideos;
        vm.getUpcomingVideos = getUpcomingVideos;
        vm.isGroupShown = isGroupShown;
        vm.toggleGroup = toggleGroup;
        vm.playVideo = playVideo;
        vm.loadVideo = loadVideo;
        vm.setDetailContent = setDetailContent;
        vm.lastObject = "";
        vm.getKnowledgeResource = getKnowledgeResource;
        vm.addQuestion = addQuestion;
        vm.addAnswer= addAnswer;
        vm.isPressed = isPressed;
        vm.deviceHeight = $window.innerHeight;
        vm.deviceWidth = $window.innerWidth;
        vm.topRowHeight = vm.deviceHeight * .37;

        vm.topRowStyle = {
            height: vm.topRowHeight
        }

        function isPressed(){
          vm.pressed=true;
        }

        function addQuestion(videoId){
          console.log(sessionConstant.login.id);
          homeservice.addQuestion(vm.questionsText,videoId,sessionConstant.login.id).then(function (response){
            homeservice.getQuestions(sessionConstant.currentImage.id).then(function (result){
              vm.queAns = [];
              console.log(result);
              vm.queAns = result;
              vm.questionsText = "";
            })
            console.log(response);
          })
        }

        function addAnswer(question){
          homeservice.addAnswer(vm.answerText,question,sessionConstant.login.id).then(function (response){
            homeservice.getQuestions(sessionConstant.currentImage.id).then(function (result){
              vm.queAns = [];
              console.log(result);
              vm.queAns = result;
              vm.questionsText = "";
              vm.answerText = "";
            })
            console.log(response);
          })
        }


        function toggleGroup(head) {
            // console.log(head);
            if (vm.isGroupShown(head)) {
                vm.shownGroup = null;
            } else {
                vm.shownGroup = head;
            }
        };
        function isGroupShown(head) {
            return vm.shownGroup === head;
        };
        function getLiveVideos(){
          console.log('in');


          homeservice.getData('live').then(function (response) {
            //console.log('in');
          console.log(response);
          vm.liveList = response;
          }, function (errorMessage) {
              console.log(errorMessage);
              //logger.feedback(errorMessage, errorMessage, 'Failed');
          });
        }

        function getUpcomingVideos(){
          console.log('in');


          homeservice.getData('upcomming').then(function (response) {
            //console.log('in');
          console.log(response);
          vm.upVideoList = response;
          }, function (errorMessage) {
              console.log(errorMessage);
              //logger.feedback(errorMessage, errorMessage, 'Failed');
          });
        }

        function setDetailContent(item) {
            sessionConstant.knowledgeResource.detailContent = item;
            $state.go('details');
        }

        function getKnowledgeResource() {
            vm.myKnowledgeData = sessionConstant.knowledgeResource.detailContent;
            //vm.myKnowledgeData.content = myHtml;
            homeservice.getDetails(vm.myKnowledgeData.id).then(function (response){
              console.log(response);
              vm.eventDetails = response;
              homeservice.getVideoList(vm.myKnowledgeData.id).then(function (result){
                vm.videoList = result;
                console.log(vm.videoList);
                for(var i in vm.videoList){
                     vm.videoList[i].url = $sce.trustAsResourceUrl(vm.videoList[i].url);
                }
              })
            })
            console.log(vm.myKnowledgeData);
            vm.ShouldShowSpinner = false
        }

        function playVideo(video) {
          sessionConstant.currentImage = video;
          $state.go('play');
        }

        function loadVideo(){
          vm.video = sessionConstant.currentImage;
          //vm.video.url = $sce.trustAsResourceUrl(vm.video.url);
          homeservice.getQuestions(vm.video.id).then(function (response){
            vm.queAns = [];
            console.log(response);
            vm.queAns = response;
          })
        }
        function getMessages() {
            messagingSerivce.getMessage().then(function (response) {
                vm.data = []
                console.log(response)
                var lengths = response.data.length
                console.log(response.data.length)
                for (var i = 0; i <= response.data.length - 1; i++) {
                    if (response.data[i].message.length < 100) {
              //          console.log(response.data[i])
                        var message = response.data[i].message;
                        var date_time = response.data[i].date_time;
                        vm.data.push({
                            'message': message,
                            'date_time': date_time
                        });
                    }
                }
                messagingSerivce.insert(vm.data.length, vm.data).then(function (response) {
                    vm.count = response
                    console.log(response)
                }, function (error) {
                    console.log(error)
                })
            }, function (erro) {
                console.log(erro)
            })
        }


        function printMe() {
            console.log("printing...")
        }

        function toggleMenu() {
            if ($ionicSideMenuDelegate.isOpen(false)) {
                vm.name = "pralok";
            }
            $ionicSideMenuDelegate.toggleLeft();
        };




    }
})();
