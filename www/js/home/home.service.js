(function () {
    'use strict';

    angular.module('app.home')
        .factory('homeservice', homeservice);

    function homeservice($http,urlservice,sessionConstant) {
        var service = {
            getData: getVideos,
            getDetails: getDetails,
            getVideoList: getVideoList,
            getQuestions: getQuestions,
            addQuestion: addQuestion,
            addAnswer: addAnswer
        };
        return service;

        function getVideos(value){
            var params = value +"~true";
            var prefix = "wbmnr";
            return $http.get(urlservice.selectUrl({searchParams: params, mode: 'simple', paged: 'N'}, prefix),
                {}
            ).then(
              function (resData) {
                console.log(resData);
               // sessionConstant.myProfile = resData.data;
                return resData.data;
              },
                function (error) {
                    throw error;
                }
            )
        }
        function getDetails(id){
          return $http.get(urlservice.formUrl(id),
          {}
        ).then(
          function (resData) {
            console.log(resData);
              return resData.data;
          },
          function (error) {
            throw error;
          }
        )
        }

        function getVideoList(id) {
          var prefix = 'mdoct';
          var params = 'webminar~'+id;
          return $http.get(urlservice.selectUrl({searchParams: params, mode: 'simple', paged: 'N'}, prefix),
              {}
          ).then(
            function (resData) {
              console.log(resData);
             // sessionConstant.myProfile = resData.data;
              return resData.data;
            },
              function (error) {
                  throw error;
              }
          )

        }

        function getQuestions(id) {
          var prefix = 'dsqueans';
          var params = 'owner~'+id;
          return $http.get(urlservice.selectUrl({searchParams: params, mode: 'simple', paged: 'N'}, prefix),
              {}
          ).then(
            function (resData) {
              console.log(resData);
             // sessionConstant.myProfile = resData.data;
              return resData.data;
            },
              function (error) {
                  throw error;
              }
          )
        }

        function addQuestion(question,videoId,userId){

          return $http.post(
            urlservice.formUrl("DSQUE"),
               {
                question:question,
                questionBy:{id:userId},
                owner:{id:videoId},
                status:null
               }
            ).then(function (response){
              console.log(response);
              return response.data;
            })
        }

        function addAnswer(answer,questionId,userId){

          return $http.post(
            urlservice.formUrl("DSANS"),
               {
                question:{id:questionId},
                answer:answer,
                answerBy:{id:userId},
                status:null
               }
            ).then(function (response){
              console.log(response);
              return response.data;
            })
        }

    }

})();
