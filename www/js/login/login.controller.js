(function () {
    'use strict';

    angular
        .module('app.login')
        .controller('loginctrl', loginctrl);


    /* @ngInject */
    function loginctrl(loginservice, sessionConstant, $ionicHistory, $state, $cordovaNetwork) {
        /*jshint validthis: true */
        var vm = this;
        vm.loginForm = {};
        vm.login = login;
        vm.goToLogin = goToLogin;
        vm.loginForm.address = "172.16.1.14";


        function login(form, formData) {
            vm.ShowShowSpinner = true;
            sessionConstant.ip = formData.address;
            if ($cordovaNetwork.isOnline()) {
                loginservice.login(formData).then(function (loginresponse) {
                    vm.ShouldShowSpinner=false;
                    console.log(loginresponse);
                  if(loginresponse){
                      console.log('logged in');
                      $state.go('tabs.home');
                      window.plugins.toast.showShortCenter('Logged in successfully!!');
                      $ionicHistory.removeBackView();

                    }
                    else{

                    }

                }, function (errorMessage) {
                    console.log(errorMessage);
                    window.plugins.toast.showShortCenter(errorMessage);
                });

            } else {
                //commonService.ionicLoadingHide()
                window.plugins.toast.showShortCenter("Please Check your Internet Connection")
            }
        }

        function goToLogin() {
            $state.go('login');
        }

    }

})();
