(function () {
    'use strict';

    angular
        .module('app.login')
        .factory('loginservice', loginservice);
    function loginservice(appUrls, urlservice, sessionConstant, $http, repositoryservice, commonService) {
        var service = {
            login: doLogin
        };

        return service;

        function onSuccess(loginresponse) {
            console.log(loginresponse);
              sessionConstant.login.cert = loginresponse.data.certificate;
              sessionConstant.login.id = loginresponse.data.id;
              console.log(sessionConstant.login.id);
              return repositoryservice.deleteAll("APP_CONFIG").then(function () {
                console.log("neuron At Deleted : ");
                return repositoryservice.insert("APP_CONFIG", [loginresponse.data.certificate,loginresponse.data.id]).then(
                  function (data) {
                    console.log("neuron data from insert : " + data);
                    return data;
                  }, function (error) {
                            console.log("Error at login ")
                            throw error;
                        }
                    );
                }, function (error) {
                    console.log(error);
                    throw error
                });
                return loginresponse;
        }

        function onError(error) {
            throw error.data.msg;
        }


        function doLogin(form) {
            //form request url
            return $http.post(
                urlservice.loginUrl(),
                {
                    username: form.username,
                    password: form.password//,
                    //timestamp : new Date().getTime()
                }
                )
                .then(onSuccess, onError);
        }


    }
})();
