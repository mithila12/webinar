(function () {
    'use strict';

    angular.module('app.core')
        .factory('httpRequestInterceptor', requestInterceptor);


    angular.module('app.core')
        .config(httpconfig);
    /* @ngInject */
    function httpconfig($httpProvider, $ionicConfigProvider) {
        $ionicConfigProvider.views.maxCache(0);
        $httpProvider.interceptors.push('httpRequestInterceptor');
    };

    /* @ngInject */
    function requestInterceptor(sessionConstant) {

        var service = {
            request: intercept
        };

        return service;

        function intercept(config) {
          config.headers['Content-Type'] = 'application/json';
          config.headers['consumer_key'] = 'TENAT00000000000000000000000000000001';
          config.headers['AUTH_CERTIFICATE'] = sessionConstant.login.cert;
            /*config.headers['Content-Type'] = 'application/x-www-form-urlencoded';
            config.transformRequest = function (obj) {
                if (obj) {
                    console.log("adding params");
                    obj.reference_id = sessionConstant.login.cert;
                    obj.timestamp = new Date().getTime();
                }
                var str = [];
                for (var p in obj)
                    str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                return str.join("&");
            }*/
            return config;
        }
    }
})();
