(function () {
    'use strict';

    angular
        .module('app.core')
        .factory('repositoryservice', repositoryservice);

    /* @ngInject */
    function repositoryservice($cordovaSQLite, dbconfig, sessionConstant, $q) {

        var insertQueries = {};
        var deleteQueries = {};
        var updateQueries = {};
        var selectQueries = {};
        var deleteAllQueries = {};
        angular.forEach(dbconfig.tables, function (table) {
            var columns = [];
            var parameters = [];
            var updateCols = [];
            angular.forEach(table.columns, function (column) {
                if (column.type.indexOf("primary key") < 0) {
                    columns.push(column.name);
                    parameters.push("?");
                    updateCols.push(column.name + "=?");
                }
            });
            var insert = 'insert into ' + table.name + ' (' + columns.join(',') + ')';
            var insertParams = '(' + parameters.join(',') + ')';
            var insertQuery = {insert: insert, params: insertParams};

            var update = 'update ' + table.name + ' set ' + updateCols.join(',') + ' where id=?';
            var deleteQ = 'delete from ' + table.name + ' where id=?';
            columns.push('id');
            var select = 'select ' + columns.join(',') + ' from ' + table.name;

            insertQueries[table.name] = insertQuery;
            updateQueries[table.name] = update;
            deleteQueries[table.name] = deleteQ;
            selectQueries[table.name] = select;
            deleteAllQueries[table.name] = "delete from " + table.name;
        });


        var service = {
            insert: insert,
            insertBulk: insertBulk,
            delete: deletefn,
            deleteAll: deleteAll,
            deleteAdhoc: deleteAdhoc,
            select: select,
            selectAdhoc: selectAdhoc,
            updateAdhoc: updateAdhoc
        };
        return service;


        function insert(table_name, parameters) {
            var iq = insertQueries[table_name];
            var q = iq.insert + ' values ' + iq.params;
            console.log(q);
            return $cordovaSQLite.execute(sessionConstant.db, q, parameters);
        };

        function insertBulk(table_name, numRows, parameters) {
            var iq = insertQueries[table_name];
            var q = iq.insert + ' values ';
            var paramsArray = [];
            for (var i = 0; i < numRows; i++) {
                paramsArray.push(iq.params);
            }
            q = q + paramsArray.join(',');
            return $cordovaSQLite.execute(sessionConstant.db, q, parameters);
        };

        function deletefn(table_name, parameters) {
            var q = deleteQueries[table_name];
            return $cordovaSQLite.execute(sessionConstant.db, q, parameters);
        };

        function deleteAll(table_name) {
            var q = deleteAllQueries[table_name];
            console.log("neuron" + q);
            return $cordovaSQLite.execute(sessionConstant.db, q, []);

        };

        function deleteAdhoc(table_name, wherecondition, params) {
            var q = deleteAllQueries[table_name] + wherecondition;
            return $cordovaSQLite.execute(sessionConstant.db, q, params);
        };

        function select(table_name, whereClause, parameters) {
            var q = selectQueries[table_name] + whereClause;
            return $cordovaSQLite.execute(sessionConstant.db, q, parameters);
        };

        function selectAdhoc(query, parameters) {
            return $cordovaSQLite.execute(sessionConstant.db, query, parameters);
        };

        function updateAdhoc(table_name, fields, parameters, whereClause, whereParams) {
            var updateCols = [];
            for (var i = 0; i < fields.length; i++) {
                updateCols.push(fields[i] + "=?");
            }
            var q = "UPDATE " + table_name + " set " + updateCols.join(",");
            if (whereClause) {
                var whereCols = [];
                for (var i = 0; i < whereClause.length; i++) {
                    whereCols.push(whereClause[i] + "=?");
                    parameters.push(whereParams[i]);
                }
                q = q + " WHERE " + whereCols.join(",");
            }
            return $cordovaSQLite.execute(sessionConstant.db, q, parameters);
        }

    }

})();
