(function () {

  'use strict'
  angular.module('app.core')
    .factory('commonService', commonService)

  function commonService($ionicLoading, $cordovaToast) {
    console.log("Common Serive ")

    var service = {
      ionicLoadingShow: ionicLoadingShow,
      ionicLoadingHide: ionicLoadingHide,
      ionicToaster: ionicToaster,
      ionicSpinner: ionicSpinner
    };
    return service;

    function ionicLoadingShow() {
      console.log("IonicLoading Show")
      $ionicLoading.show({
        hideOnStateChange: true,
        delay :500
      }).then(function () {
        console.log("The loading indicator is now displayed");
      })
    }

    function ionicLoadingHide() {
      console.log("Ionic Loading Hide")
      $ionicLoading.hide().then(function () {
        console.log("The loading indicator is now hidden");
      })
    }

    function ionicSpinner() {
      console.log("Ionic Spinner")
    }

    function ionicToaster(Message) {
      $cordovaToast
        .show(Message, 'long', 'center')
        .then(function (success) {
        }, function (error) {
        });
      console.log("Ionic Toaster")
    }
  }
})
();
