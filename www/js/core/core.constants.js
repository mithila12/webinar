/* global toastr:false, moment:false */
(function () {
    'use strict';

    angular
        .module('app.core')
        .constant('errorCodes', {
            "no_network": 200,
            "need_online": "You need to be connected to the network to access this information"
        })
        .constant('urlConfig', {
            //  "app_url": "dataqube.in/wdm-pm-api",
          "login_url":":8080/webapp/rest/v1.0/core/login",
          "app_url":":8080/webapp/rest/v1.0/core/framework"

        })
        .constant('appUrls', {
            login: {
                login: 'login'
            },
            doctor:{
              saveDoctor: 'DOCTR'
            },
            patient:{
              savePatient:'PATNT'
            },
            register: {
                register: 'patient-registration'
            },
            profile: {
                getInfo: 'user-info',
                update: 'update-profile'
            },
            medicine:{
                addMedicine:'MEDIC'
            },
            lifestyle:{
                addLifestyle:'PATLS'
            },
            knowledgeResource: {
                knowledge: 'json=get_category_posts&'
            },
            patients: {
                getPatients: 'get-patients',
                getReports: 'get-report',
                getAppointments: 'get-appointment',
                sentPatientReport: 'add-report'
            },
            notification: {
                getNotification: 'notifications-for-patients'
            }
        })
        .constant('jsonConfig', {
            diet: {
                dietPlan: 'jsonConfig/dietPlanner.json'
            },
            faqs: {
              faqs: 'jsonConfig/faqs.json'
            }
        })
        .constant('dbconfig', {
            name: 'dip.db',
            tables: [
                {
                    name: 'APP_CONFIG',
                    columns: [
                        {name: 'id', type: 'integer primary key'},
                        {name: 'reference_id', type: 'text'},
                        {name: 'loginId', type:'text'}
                    ]
                },
                {
                    name: "ALARMS",
                    columns: [
                        {name: "id", type: "integer primary key"},
                        {name: "alarm_id", type: "text unique"},
                        {name: "title", type: "text"},
                        {name: "datetime", type: "text"}
                    ]
                }, {
                    name: "MESSAGE",
                    columns: [
                        {name: "id", type: "integer primary key"},
                        {name: "template", type: "text"},
                        {name: "date", type: "text"},
                    ]
                }, {
                    name: "WELCOME",
                    columns: [
                      {name: "id", type: "integer primary key"},
                      {name: "show", type: "text"}
                    ]
                }]
        })
        .value("sessionConstant", {
            ip:"",
            db: null,
            login: {},
            currentPatient: {},
            myProfile: {},
            list: {},
            message: {},
            knowledgeResource: {
                detailContent: ""
            },
            answers:{},
            infoToUpdate:{},
            loginInfo:{},
            units:{},
            image: null,
            reportType: {
              GFR: {},
              UrineAlbumin: {},
              BloodUrea: {},
              Serum: {},
              Cholesterol: {},
              HDL: {},
              Triglycerides: {},
              vldl: {},
              LDL: {},
              sbp: {},
              dbp: {},
              randomSugar: {},
              postmeal: {},
              fastingsugar: {},
              hba1c: {}
            },
            testReport:{},
            overview:{},
            questionsToUpdate:{},
            myInfo:{},
            currentImage:{}
        });
})();
