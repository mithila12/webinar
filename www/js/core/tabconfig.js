(function () {
    'use strict';

    angular.module('app.core')
        .config(tabconfig);
    function tabconfig($ionicConfigProvider) {
        $ionicConfigProvider.tabs.position('bottom');
    };
})();
