(function () {

    angular
        .module('app.core')
        .factory('sameValidator', sameValidator)
        .config(validations, sameValidator)

    function sameValidator() {
        return {
            name: 'password',
            validate: function (value, args) {

                return value === document.querySelector('[name="' + args.as + '"]').value;
            }
        };
    };

    /* @ngInject */
    function validations(valdrProvider) {

        valdrProvider.addValidator('sameValidator');
        valdrProvider.addConstraints({
            'Register': {
                'doctorID': {
                    'required': {
                        'message': 'Doctor Id is required.'
                    }
                },
                'firstName': {
                    'size': {
                        'min': 3,
                        'max': 20,
                        'message': 'First name is required to be between 3 and 20 characters.'
                    },
                    'required': {
                        'message': 'First name is required.'
                    }
                },
                'lastName': {
                    'size': {
                        'min': 3,
                        'max': 20,
                        'message': 'Last name is required to be between 3 and 20 characters.'
                    },
                    'required': {
                        'message': 'Last name is required.'
                    }
                },
                'gender': {
                    'required': {
                        'message': 'Please select Gender.'
                    }
                },
                'email': {
                    'email': {
                        'message': 'Not a valid email address'
                    },
                    'required': {
                        'message': 'Email is required.'
                    }
                },
                'username': {
                    'required': {
                        'message': 'Username is required.'
                    }
                },
                'contact': {
                    'min': {
                        'value': 1000000000,
                        'message': 'Contact no should be exactly 10 digits.'
                    },
                    'max': {
                        'value': 9999999999,
                        'message': 'Contact no should be exactly 10 digits.'
                    },
                    'required': {
                        'message': 'Contact no is required.'
                    }
                },
                'eContact': {
                    'min': {
                        'value': 1000000000,
                        'message': 'Emergency Contact no should be exactly 10 digits.'
                    },
                    'max': {
                        'value': 9999999999,
                        'message': 'Emergency Contact no should be exactly 10 digits.'
                    },
                    'required': {
                        'message': 'Emergency Contact no is required.'
                    }
                },
                "password": {
                    "required": {
                        "message": "Password is required."
                    }
                },
                "cPassword": {
                    "required": {
                        "message": "Password is required."
                    },
                    "password": {
                        "as": "password",
                        "message": "Password does not match."
                    },
                },
            },
            'Login': {
                'username': {
                    'required': {
                        'message': 'Username is required.'
                    }
                },
                'password': {
                    'required': {
                        'message': 'Password is required.'
                    }
                },

            },
            'editProfile': {
                'firstName': {
                    'size': {
                        'min': 3,
                        'max': 20,
                        'message': 'First name is required to be between 3 and 20 characters.'
                    },
                    'required': {
                        'message': 'First name is required.'
                    }
                },
                'lastName': {
                    'size': {
                        'min': 3,
                        'max': 20,
                        'message': 'Last name is required to be between 3 and 20 characters.'
                    },
                    'required': {
                        'message': 'Last name is required.'
                    }
                },
                'gender': {
                    'required': {
                        'message': 'Please select Gender.'
                    }
                },
                'email': {
                    'email': {
                        'message': 'Not a valid email address'
                    },
                    'required': {
                        'message': 'Email is required.'
                    }
                },
                'contact_no': {
                    'min': {
                        'value': 1000000000,
                        'message': 'Contact no should be exactly 10 digits.'
                    },
                    'max': {
                        'value': 9999999999,
                        'message': 'Contact no should be exactly 10 digits.'
                    },
                    'required': {
                        'message': 'Contact no is required.'
                    }
                },
                'contact_emergency': {
                    'min': {
                        'value': 1000000000,
                        'message': 'Emergency Contact no should be exactly 10 digits.'
                    },
                    'max': {
                        'value': 9999999999,
                        'message': 'Emergency Contact no should be exactly 10 digits.'
                    },
                    'required': {
                        'message': 'Emergency Contact no is required.'
                    }
                },
                'dob': {
                    'required': {
                        'message': 'Date of birth is required.'
                    },
                    'past': {
                        'message': 'Date should not be greater than todays date.'
                    }
                }
            },
                'Alarm': {
                'alarmTitle':{
                    'required':{
                        'message': 'Reminder Title is required.'
                      }

                  },
                  },



            'ProfileUpdate': {
                'patient_name': {
                    'required':{
                        'message': 'Name is required'
                    }
                },

                'patient_dtype': {
                    'required':{
                        'message': 'Diabetes Type is required'
                    }
                },

                'patient_gender': {
                    'required':{
                        'message': 'Gender is required'
                    }
                },


                'patient_age': {
                    'required':{
                        'message': 'Age is required'
                    }
                },

                'patient_weight': {
                    'required':{
                        'message': 'Weight is required'
                    }
                },

                'patient_height': {
                    'required':{
                        'message': 'Height is required'
                    }
                },

                'patient_phoneNo': {
                    'min': {
                        'value': 1000000000,
                        'message': 'Phone no should be exactly 10 digits.'
                    },
                    'max': {
                        'value': 9999999999,
                        'message': 'Phone no should be exactly 10 digits.'
                    },
                    'required': {
                        'message': 'Phone no is required.'
                    }

                },

                },



            'BasicInfo':{

                'gender': {
                    'required':{
                        'message': 'Gender is required'
                    }
                },


                'age': {
                    'required':{
                        'message': 'Age is required'
                    }
                },

                'weight': {
                    'required':{
                        'message': 'Weight is required'
                    }
                },

                'height': {
                    'required':{
                        'message': 'Height is required'
                    }
                },

                'phoneNo': {
                    'min': {
                        'value': 1000000000,
                        'message': 'Phone no should be exactly 10 digits.'
                    },
                    'max': {
                        'value': 9999999999,
                        'message': 'Phone no should be exactly 10 digits.'
                    },
                    'required': {
                        'message': 'Phone no is required.'
                    },

                },

            },

            'Medication':{
                'mediName':{
                    'required':{
                        'message': 'Name is required'
                    }
                },

                'route':{
                    'required':{
                        'message':'Route is required'
                    }
                },

                'dose':{
                    'required':{
                        'message':'Dose is required'
                    }
                }
            },

            'Record':{
                'hba1c':{
                    'required':{
                        'message': 'HBA1c is required'
                    }
                },

                'postmeal':{
                    'required':{
                        'message':'Postmeal Sugar is required'
                    }
                },

                'fasting':{
                    'required':{
                        'message':'Fasting sugar is required'
                    }
                },
                'random':{
                    'required':{
                        'message': 'Random sugar is required'
                    }
                },

                'sbp':{
                    'required':{
                        'message':'Blood pressure is required'
                    }
                },

                'dbp':{
                    'required':{
                        'message':'Blood pressure is required'
                    }
                },
                'ldl':{
                    'required':{
                        'message': 'HBA1c is required'
                    }
                },

                'tri':{
                    'required':{
                        'message':'Postmeal Sugar is required'
                    }
                },

                'hdl':{
                    'required':{
                        'message':'Fasting sugar is required'
                    }
                },
                'chole':{
                    'required':{
                        'message': 'Random sugar is required'
                    }
                },

                'ratio':{
                    'required':{
                        'message':'Blood pressure is required'
                    }
                },

                'serum':{
                    'required':{
                        'message':'Blood pressure is required'
                    }
                },
                'urea':{
                    'required':{
                        'message':'Postmeal Sugar is required'
                    }
                },

                'egfr':{
                    'required':{
                        'message':'Fasting sugar is required'
                    }
                },
                'testName':{
                    'required':{
                        'message': 'Random sugar is required'
                    }
                },

                'outcome':{
                    'required':{
                        'message':'Blood pressure is required'
                    }
                },
                'date':{
                    'required':{
                        'message': 'Date is required'
                    }
                }
            }





        });
    }
})();
